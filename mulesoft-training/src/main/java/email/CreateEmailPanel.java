package email;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

@Component
public class CreateEmailPanel extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	@Autowired
	private MulesoftRESTClient urls;
	private UserResponse userResponse;
	private Button sendMail = new Button("Send", VaadinIcon.ARROW_RIGHT.create());
	private Button cancelMail = new Button("Cancel", VaadinIcon.STOP.create());
	private ComboBox<String> to = new ComboBox<String>("To");
	private TextField subject = new TextField("Subject");
	private TextArea body = new TextArea("Contents");

	public HorizontalLayout buttonPanel;

	@PostConstruct
	protected void init() {
		removeAll();
		if (userResponse != null) {
			to.setItems(names(urls.users(userResponse.getName())));
		}
		setWidth("800px");
		setHeight("400px");
		body.setSizeFull();

		buttonPanel = VaadinUtils.horiz(sendMail, cancelMail);
		add(VaadinUtils.vert(VaadinUtils.horiz(to, subject), body), buttonPanel);
		this.setSizeFull();

	}

	@SuppressWarnings("unused")
	private ComponentEventListener<ClickEvent<Button>> cancelMail() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				// TODO Auto-generated method stub

			}

		};
	}

	@SuppressWarnings("unused")
	private ComponentEventListener<ClickEvent<Button>> sendMail() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (urls != null) {
					String two = to.getValue();
					String from = userResponse.getName();
					String sub = subject.getValue();
					String bod = body.getValue();
					urls.createMail(userResponse, two, from, sub, bod);
				}
			}

		};
	}

	private Collection<String> names(List<User> users) {
		Collection<String> names = new ArrayList<>();
		for (User user : users) {
			names.add(user.getName());
		}
		return names;
	}

	public void urls(MulesoftRESTClient urls) {
		this.urls = urls;
	}

	public MulesoftRESTClient getUrls() {
		return urls;
	}

	public void setUrls(MulesoftRESTClient urls) {
		this.urls = urls;
	}

	public UserResponse getUserResponse() {
		return userResponse;
	}

	public void setUserResponse(UserResponse userResponse) {
		this.userResponse = userResponse;
		init();
	}

	public Button getSendMail() {
		return sendMail;
	}

	public void setSendMail(Button sendMail) {
		this.sendMail = sendMail;
	}

	public Button getCancelMail() {
		return cancelMail;
	}

	public void setCancelMail(Button cancelMail) {
		this.cancelMail = cancelMail;
	}

	public ComboBox<String> getTo() {
		return to;
	}

	public void setTo(ComboBox<String> to) {
		this.to = to;
	}

	public TextField getSubject() {
		return subject;
	}

	public void setSubject(TextField subject) {
		this.subject = subject;
	}

	public TextArea getBody() {
		return body;
	}

	public void setBody(TextArea body) {
		this.body = body;
	}
}
