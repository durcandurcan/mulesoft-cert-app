package email;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class MulesoftRESTClient {
	@Value("${updateemail.url}")
	private String UPDATE_EMAIL_URL;
	@Value("${login.url}")
	private String LOGIN_URL;
	@Value("${createaccount.url}")
	private String CREATEACCOUNT_URL;
	@Value("${user.url}")
	private String USERS_URL;
	@Value("${email.url}")
	private String EMAILS_URL;
	@Value("${create.email.url}")
	private String CREATE_EMAILS_URL;
	@Value("${allemails.url}")
	private String ALL_EMAILS_URL;
	@Value("${deleteemail.url}")
	private String DELETE_EMAILS_URL;
	@Autowired
	private RestTemplateBuilder restTemplate;

	private UserResponse createUserResponse(String userName, String password, boolean result, Exception e) {
		UserResponse ur = new UserResponse();
		ur.setName(userName);
		ur.setPassword(password);
		ur.setReason("Exception " + e.getClass().getSimpleName());
		ur.setResult(false);
		return ur;
	}

	public UserResponse login(String un, String pwd) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(LOGIN_URL).queryParam("userName", un)
				.queryParam("password", pwd);

		try {
			HttpEntity<UserResponse> response = restTemplate.build().exchange(builder.toUriString(), HttpMethod.GET,
					null, UserResponse.class);
			return response.getBody();
		} catch (Exception e) {
			return createUserResponse(un, pwd, false, e);
		}
	}

	public UserResponse createUser(String un, String pwd) {
		Map<String, String> params = new HashMap<String, String>();
		if (pwd != null) {
			params.put("password", pwd);
		}
		if (un != null) {
			params.put("userName", un);
		}
		params.put("action", "create");
		try {
			UserResponse rsp = restTemplate.build().postForObject(CREATEACCOUNT_URL, params, UserResponse.class);
			return rsp;
		} catch (Exception e) {
			return createUserResponse(un, pwd, false, e);
		}
	}

	public List<User> users(String user) {
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(USERS_URL).queryParam("userName", user);
			HttpEntity<User[]> response = restTemplate.build().exchange(builder.toUriString(), HttpMethod.GET, null,
					User[].class);
			
			return Arrays.asList(response.getBody());
		} catch (Exception e) {
		}

		return new ArrayList<>();
	}

	public List<Email> getEmailsForUser(String user, String password, long... deletedValues) {
		try {
			Set<Long> values = new TreeSet<Long>();
			for (long deletedValue : deletedValues) {
				values.add(Long.valueOf(deletedValue));
			}
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(EMAILS_URL).queryParam("userName", user)
					.queryParam("password", password).queryParam("deleted", "1");
			HttpEntity<Email[]> response = restTemplate.build().exchange(builder.toUriString(), HttpMethod.GET, null,
					Email[].class);
			Email[] emails = response.getBody();
			List<Email> filter = new ArrayList<>();
			for (Email email : emails) {
				if (values.contains(email.getDeleted())) {
					filter.add(email);
				}
			}
			return filter;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return new ArrayList<>();
	}

	public CreateMailResponse createMail(UserResponse ur, String two, String from, String sub, String bod) {
		Map<String, String> params = new HashMap<String, String>();
		CreateMailResponse rsp;
		params.put("userName", ur.getName());
		params.put("password", ur.getPassword());
		if (from != null) {
			params.put("frm", from);
		}
		if (sub != null) {
			params.put("subject", sub);
		}
		if (bod != null) {
			params.put("body", bod);
		}
		if (two != null) {
			params.put("too", two);
		}
		params.put("read", "false");
		params.put("id", UUID.randomUUID().toString());
		try {
			rsp = restTemplate.build().postForObject(CREATE_EMAILS_URL, params, CreateMailResponse.class);
		} catch (Exception e) {
			rsp = new CreateMailResponse();
			rsp.setResult(false);
			rsp.setDescription(e.getMessage());
		}
		return rsp;
	}

	public boolean updateEmail(Email email, UserResponse ur) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", email.getId());
		params.put("userName", ur.getName());
		params.put("password", ur.getPassword());
		params.put("deleted", email.getDeleted()+"");
		params.put("readCount", email.getReadCount()+"");
		try {
			restTemplate.build().postForObject(UPDATE_EMAIL_URL, params, CreateMailResponse.class);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean deleteEmail(Email email, UserResponse ur) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", email.getId());
		params.put("userName", ur.getName());
		params.put("password", ur.getPassword());
		try {
			restTemplate.build().postForObject(DELETE_EMAILS_URL, params, CreateMailResponse.class);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Email> getAllEmailsForUser(String user, String password) {
		try {

			Map<String, String> params = new HashMap<String, String>();
			params.put("password", password);
			params.put("user", user);
			params.put("all", "true");

			ResponseEntity<Email[]> resp = restTemplate.build().postForEntity(EMAILS_URL, params, Email[].class);
			Email[] ret = resp.getBody();
			List<Email> filter = new ArrayList<>();
			for (int i = 0; i < ret.length; i++) {
				if (ret[i].getTo().equals(user)) {
					filter.add(ret[i]);
				}
			}
			return filter;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return new ArrayList<>();
	}
}
