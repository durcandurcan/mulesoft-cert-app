package email;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final String MAINVIEW = "main";

	@Autowired
	private MainPanel lp;

	
	private void handleResponse(UserResponse rsp) {
		String msg = "Action " + rsp.getAction();
		if (!rsp.isResult()) {
			if (rsp.getReason() != null) {
				msg = msg + rsp.getReason();
			} else {
				msg = msg + " Failed";
			}
			Notification.show(msg);
		}
	}

	@PostConstruct
	public void init() {
		lp.handler(new ResponseHandler() {

			@Override
			public void handle(UserResponse ur) {
				handleResponse(ur);
			}

		});
		add(lp);
	}

}
