package email;

public class Email {
	private String to;
	private String from;
	private String subject;
	private long deleted;
	private String body;
	private String id;
	private long readCount;
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public long getDeleted() {
		return deleted;
	}
	public void setDeleted(long deleted) {
		this.deleted = deleted;
	}
	public boolean isDeleted() {
		if(deleted == 0) {
			return false;
		}
		return true;
	}
	public String getId() {
		return id;
	}
	public void setUuid(String uuid) {
		this.id = uuid;
	}
	@Override
	public String toString() {
		return "Email [to=" + to + ", from=" + from + ", subject=" + subject + ", deleted=" + deleted + ", body=" + body
				+ ", id=" + id + "]";
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public long getReadCount() {
		return readCount;
	}
	public void setReadCount(long readCount) {
		this.readCount = readCount;
	}
	
}
