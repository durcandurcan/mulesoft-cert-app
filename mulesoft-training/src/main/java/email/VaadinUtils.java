package email;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

public class VaadinUtils {
	public static boolean isEmpty(TextField tf) {
		String val = tf.getValue();
		if (val == null || val.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(PasswordField tf) {
		String val = tf.getValue();
		if (val == null || val.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static HorizontalLayout horiz(com.vaadin.flow.component.Component... components) {
		HorizontalLayout horiz = new HorizontalLayout();
		for (com.vaadin.flow.component.Component component : components) {
			horiz.add(component);
		}
		return horiz;
	}

	public static VerticalLayout vert(com.vaadin.flow.component.Component... components) {
		VerticalLayout horiz = new VerticalLayout();
		for (com.vaadin.flow.component.Component component : components) {
			horiz.add(component);
		}
		return horiz;
	}
	
	public static HorizontalLayout horizPair(Component one, Component two) {
		return horiz(one,two);
	}
}
