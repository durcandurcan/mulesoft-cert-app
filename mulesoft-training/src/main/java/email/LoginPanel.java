package email;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

@Component
public class LoginPanel extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Label user = new Label("User Name");
	private TextField userName = new TextField("User Name");
	private Label pwd = new Label("Password");
	private PasswordField pwf = new PasswordField("Password");
	private Button login = new Button("Login", VaadinIcon.ALARM.create());
	private Button create = new Button("Create", VaadinIcon.BOMB.create());
	private Button showUsers = new Button("Show Users", VaadinIcon.OPEN_BOOK.create());
	private Button logout = new Button("Log Out", VaadinIcon.ARROW_BACKWARD.create());
	private Label status = new Label("Not Logged In");
	private HorizontalLayout loginButtons;

	@PostConstruct
	protected void init() {
		loginButtons = VaadinUtils.horiz(login, create);

		logout.setEnabled(false);

		showUsers.setEnabled(false);

		add(status);

	}

	public Label getUser() {
		return user;
	}

	public void setUser(Label user) {
		this.user = user;
	}

	public TextField getUserName() {
		return userName;
	}

	public void setUserName(TextField userName) {
		this.userName = userName;
	}

	public Label getPwd() {
		return pwd;
	}

	public void setPwd(Label pwd) {
		this.pwd = pwd;
	}

	public PasswordField getPwf() {
		return pwf;
	}

	public void setPwf(PasswordField pwf) {
		this.pwf = pwf;
	}

	public Button getLogin() {
		return login;
	}

	public void setLogin(Button login) {
		this.login = login;
	}

	public Button getCreate() {
		return create;
	}

	public void setCreate(Button create) {
		this.create = create;
	}

	public Button getShowUsers() {
		return showUsers;
	}

	public void setShowUsers(Button showUsers) {
		this.showUsers = showUsers;
	}

	public Button getLogout() {
		return logout;
	}

	public void setLogout(Button logout) {
		this.logout = logout;
	}

	public Label getStatus() {
		return status;
	}

	public void setStatus(Label status) {
		this.status = status;
	}

	public HorizontalLayout getLoginButtons() {
		return loginButtons;
	}

	public void setLoginButtons(HorizontalLayout loginButtons) {
		this.loginButtons = loginButtons;
	}
}
