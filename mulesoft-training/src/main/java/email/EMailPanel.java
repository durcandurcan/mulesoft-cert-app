package email;

import static email.VaadinUtils.horiz;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.data.selection.SelectionListener;

@Component
public class EMailPanel extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<Email> grid;
	private Button open;
	private Button delete;
	private Button create;
	private Button reply;
	private Button refresh;
	private Checkbox getAllButton;
	private Checkbox displayEmailOnClick;
	private UserResponse ur;
	private CreateEmailPanel cep;
	private ReplyEmailPanel rep;
	private OpenEmailPanel oep;
	@Autowired
	private MulesoftRESTClient urls;
	private List<Email> emails;

	public Grid<Email> grid(List<Email> emails) {
		this.emails = emails;
		if (grid != null) {
			selectionListener = null;
			grid = null;
		}
		

		grid = new Grid<Email>();
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.addColumn(Email::getFrom).setHeader("From");
		grid.addColumn(Email::getTo).setHeader("To");
		grid.addColumn(Email::getSubject).setHeader("Subject");
		grid.addColumn(Email::getReadCount).setHeader("Read");
		grid.addColumn(Email::getDeleted).setHeader("Deleted");
		grid.setItems(emails);
		grid.setWidth("1000px");
		grid.setHeight("300px");

		grid.addSelectionListener(selectionListener());
		if (emails != null && !emails.isEmpty()) {
			displayEmailOnClick.setEnabled(true);
		} else {
			displayEmailOnClick.setEnabled(false);
		}
		return grid;
	}

	private Email selectedEmail;

	protected void handleSelectedEmail(Email email) {
		delete.setEnabled(true);
		reply.setEnabled(true);
		open.setEnabled(true);
		selectedEmail = email;

		if(selectedEmail.isDeleted()) {
			delete.setEnabled(false);
		} else {
			delete.setEnabled(true);
		}
		if (displayEmailOnClick.getValue()) {
			if (emails != null && !emails.isEmpty()) {
				openSelectedEmail();
			}
		}
	}

	private SelectionListener<Grid<Email>, Email> selectionListener;

	private SelectionListener<Grid<Email>, Email> selectionListener() {
		if (selectionListener == null) {
			return selectionListener = new SelectionListener<Grid<Email>, Email>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void selectionChange(SelectionEvent<Grid<Email>, Email> event) {
					Optional<Email> item = event.getFirstSelectedItem();
					if (item.isPresent()) {
						handleSelectedEmail(item.get());
					}
				}

			};
		}
		return selectionListener;
	}

	public EMailPanel userResponse(UserResponse ur) {
		this.ur = ur;
		return this;
	}

	public EMailPanel() {
		this.setSizeFull();
	}

	private HorizontalLayout buttonsPanel;

	private HorizontalLayout buttons() {
		if (buttonsPanel == null) {
			open = new Button("Open", VaadinIcon.INBOX.create());
			open.addClickListener(openEmail());
			open.setEnabled(false);
			delete = new Button("Delete", VaadinIcon.DEL.create());
			delete.addClickListener(deleteEmail());
			delete.setEnabled(false);
			reply = new Button("Reply", VaadinIcon.REPLY.create());
			reply.addClickListener(replyEmial());
			reply.setEnabled(false);
			create = new Button("New", VaadinIcon.EDIT.create());
			create.addClickListener(createEmail());

			refresh = new Button("Refresh", VaadinIcon.REFRESH.create());
			refresh.addClickListener(refreshEmails());
			getAllButton = new Checkbox("Refresh With Get All");
			getAllButton.addClickListener(getAllClicked());
			displayEmailOnClick = new Checkbox("Display Email On Select");
			buttonsPanel = horiz(open, reply, create, delete, getAllButton, refresh, displayEmailOnClick);
			buttonsPanel.setWidth("1000px");
		}
		if (emails == null || emails.isEmpty()) {
			open.setEnabled(false);
			delete.setEnabled(false);
			reply.setEnabled(false);
			displayEmailOnClick.setEnabled(false);
		} else {
			displayEmailOnClick.setEnabled(true);
		}
		create.setEnabled(true);
		refresh.setEnabled(true);
		return buttonsPanel;
	}

	private boolean getAllEmails = false;

	private ComponentEventListener<ClickEvent<Checkbox>> getAllClicked() {
		return new ComponentEventListener<ClickEvent<Checkbox>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Checkbox> event) {
				Checkbox cb = event.getSource();
				getAllEmails = cb.getValue();
				refreshEmailGrid();
			}

		};
	}

	private void refreshEmailGrid() {
		if (grid != null) {
			remove(grid);
		}
		if (ur != null) {
			if (getAllEmails) {
				add(grid(urls.getEmailsForUser(ur.getName(), ur.getPassword(), 0L, 1L)));
			} else {
				add(grid(urls.getEmailsForUser(ur.getName(), ur.getPassword(), 0L)));
			}
		}
	}

	private ComponentEventListener<ClickEvent<Button>> refreshEmails() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				refreshEmailGrid();
			}

		};
	}

	@SuppressWarnings("unused")
	private ComponentEventListener<ClickEvent<Button>> getAllEmails() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (grid != null) {
					remove(grid);
				}
				if (ur != null) {
					add(grid(urls.getAllEmailsForUser(ur.getName(), ur.getPassword())));
				}
			}

		};
	}

	protected ComponentEventListener<ClickEvent<Button>> cancelEMailClick() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				cep.setVisible(false);
				setEmails(emails);
			}

		};
	}

	protected ComponentEventListener<ClickEvent<Button>> cancelOpenEMailClick() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				oep.setVisible(false);
				setEmails(emails);
			}

		};
	}

	protected ComponentEventListener<ClickEvent<Button>> cancelReplyEMailClick() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				rep.setVisible(false);
				setEmails(emails);
			}

		};
	}

	protected ComponentEventListener<ClickEvent<Button>> sendEmailClick() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (urls != null) {
					String two = cep.getTo().getValue();
					String from = cep.getUserResponse().getName();
					String sub = cep.getSubject().getValue();
					String bod = cep.getBody().getValue();
					CreateMailResponse rsp = urls.createMail(ur,two, from, sub, bod);
					if (rsp == null) {
						Notification.show("Failed to send email to " + two);
					}
				}
				cep.setVisible(false);
				setEmails(emails);
				refreshEmailGrid();
			}

		};
	}

	protected ComponentEventListener<ClickEvent<Button>> sendReplyEmailClick() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (urls != null) {
					String two = rep.getTo().getText();
					String from = rep.getUserResponse().getName();
					String sub = rep.getSubject().getText();
					String bod = rep.getBody().getValue();
					urls.createMail(ur,two, from, sub, bod);
				}
				rep.setVisible(false);
				setEmails(emails);
				refreshEmailGrid();
			}

		};
	}

	private ComponentEventListener<ClickEvent<Button>> createEmail() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				EMailPanel.this.removeAll();
				if (cep == null) {
					cep = new CreateEmailPanel();
					cep.getSendMail().addClickListener(sendEmailClick());
					cep.getCancelMail().addClickListener(cancelEMailClick());
				}
				cep.urls(urls);
				cep.setUserResponse(ur);
				EMailPanel.this.add(cep);
				cep.init();
				cep.setVisible(true);
			}

		};
	}

	public void displayCreateEmailPanel(boolean flg) {
		if (cep != null) {
			cep.setVisible(flg);
		}
	}

	private ComponentEventListener<ClickEvent<Button>> replyEmial() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				EMailPanel.this.removeAll();
				if (rep == null) {
					rep = new ReplyEmailPanel(selectedEmail);
					rep.urls(urls);
					rep.setUserResponse(ur);
					rep.getSendMail().addClickListener(sendReplyEmailClick());
					rep.getCancelMail().addClickListener(cancelReplyEMailClick());
				}
				EMailPanel.this.add(rep);
				rep.init();
				rep.setVisible(true);
			}

		};
	}

	private ComponentEventListener<ClickEvent<Button>> deleteEmail() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (selectedEmail != null) {
					
					boolean ret = urls.deleteEmail(selectedEmail, ur);
					if (!ret) {
						Notification.show("Failed to delete email id " + selectedEmail.getId());
						return;
					}
					refreshEmailGrid();
					delete.setEnabled(false);
					reply.setEnabled(false);
					open.setEnabled(false);
					selectedEmail = null;
				}
			}

		};
	}

	private void openSelectedEmail() {
		EMailPanel.this.removeAll();
		if (oep == null) {
			oep = new OpenEmailPanel(selectedEmail);
			oep.urls(urls);
			oep.setUserResponse(ur);
			oep.getSendMail().setVisible(false);
			oep.getCancelMail().addClickListener(cancelOpenEMailClick());
		}
		EMailPanel.this.add(oep);
		oep.initSelectedEmail(selectedEmail);
		selectedEmail.setReadCount(selectedEmail.getReadCount()+1);
		urls.updateEmail(selectedEmail,ur);
		oep.setVisible(true);
	}

	private ComponentEventListener<ClickEvent<Button>> openEmail() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				EMailPanel.this.removeAll();
				if (oep == null) {
					oep = new OpenEmailPanel(selectedEmail);
					
				}
				selectedEmail.setReadCount(selectedEmail.getReadCount()+1);
				urls.updateEmail(selectedEmail,ur);
				EMailPanel.this.add(oep);
				oep.urls(urls);
				oep.setUserResponse(ur);
				oep.getSendMail().setVisible(false);
				oep.getCancelMail().addClickListener(cancelOpenEMailClick());
				oep.init();
				oep.setVisible(true);
			}

		};
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
		if (buttonsPanel != null) {
			remove(buttonsPanel);
		}
		add(buttons());
		if (grid != null) {
			remove(grid);
		}
		add(grid(emails));
	}

	public MulesoftRESTClient getUrls() {
		return urls;
	}

	public void setUrls(MulesoftRESTClient urls) {
		this.urls = urls;
	}
}
