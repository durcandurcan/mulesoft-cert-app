package email;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;

public class OpenEmailPanel extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	@Autowired
	private MulesoftRESTClient urls;
	private UserResponse userResponse;
	Button sendMail = new Button("Send", VaadinIcon.ARROW_RIGHT.create());
	Button cancelMail = new Button("Cancel", VaadinIcon.STOP.create());
	Label to = new Label("To");
	Label subject = new Label("Subject");

	TextArea body = new TextArea("Contents");

	private String replyTo;
	private String replySubject;
	private String replyBody;
	private String from;

	public HorizontalLayout buttonPanel;

	public OpenEmailPanel(String replyTo, String replySubject, String replyBody, String from) {
		this.replyBody = replyBody;
		this.replyTo = replyTo;
		this.replySubject = replySubject;
		this.from = from;
		init();
	}

	public OpenEmailPanel(Email email) {
		this(email.getTo(), email.getSubject(), email.getBody(), email.getFrom());
	}

	public OpenEmailPanel initSelectedEmail(Email email) {
		this.replyBody = email.getBody();
		this.replyTo = email.getTo();
		this.replySubject = email.getSubject();
		this.from = email.getFrom();
		init();
		return this;
	}
	@PostConstruct
	protected void init() {
		removeAll();
		to.setText(replyTo);
		subject.setText("RE:" + replySubject);
		setWidth("800px");
		setHeight("400px");
		body.setSizeFull();
		body.setValue(replyBody);
		// sendMail.addClickListener(sendMail());
		// cancelMail.addClickListener(cancelMail());
		sendMail.setEnabled(false);
		buttonPanel = VaadinUtils.horiz(sendMail, cancelMail);
		add(VaadinUtils.vert(VaadinUtils.horiz(to, subject), body), buttonPanel);
		this.setSizeFull();

	}

	public void urls(MulesoftRESTClient urls) {
		this.urls = urls;
	}

	public MulesoftRESTClient getUrls() {
		return urls;
	}

	public void setUrls(MulesoftRESTClient urls) {
		this.urls = urls;
	}

	public UserResponse getUserResponse() {
		return userResponse;
	}

	public void setUserResponse(UserResponse userResponse) {
		this.userResponse = userResponse;
	}

	public Button getSendMail() {
		return sendMail;
	}

	public void setSendMail(Button sendMail) {
		this.sendMail = sendMail;
	}

	public Button getCancelMail() {
		return cancelMail;
	}

	public void setCancelMail(Button cancelMail) {
		this.cancelMail = cancelMail;
	}

	public TextArea getBody() {
		return body;
	}

	public void setBody(TextArea body) {
		this.body = body;
	}

	public Label getTo() {
		return to;
	}

	public void setTo(Label to) {
		this.to = to;
	}

	public Label getSubject() {
		return subject;
	}

	public void setSubject(Label subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}
}
