package email;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

@Component
public class MainPanel extends VerticalLayout {
	
	private static final long serialVersionUID = 1L;
	protected static final String MAINVIEW = "main";
	
	@Autowired
	private MulesoftRESTClient urls;

	private EMailPanel ep;
	private UserResponse me;

	private ResponseHandler handler;
	private HorizontalLayout loginButtons;

	private TextField userName = new TextField("User Name");
	private PasswordField pwf = new PasswordField("Password");
	private Button login = new Button("Login", VaadinIcon.ALARM.create());
	private Button create = new Button("Create", VaadinIcon.BOMB.create());
	private Button logout = new Button("Log Out", VaadinIcon.ARROW_BACKWARD.create());
	private Label status = new Label("Not Logged In");
	private VerticalLayout loginPanel;
	private VerticalLayout emailAndListUsersPanel;
	@SuppressWarnings("unused")
	private HorizontalLayout panels;
	
	public void handler(ResponseHandler hand) {
		this.handler = hand;
	}

	private void handle(UserResponse rsp) {
		handler.handle(rsp);
		ep.userResponse(rsp);
	}

	protected void removeEmailPanel() {
		if (ep != null) {
			if (emailAndListUsersPanel != null) {
				ep.displayCreateEmailPanel(false);
				ep.setVisible(false);
			}
		}
	}

	private void initEmailPanel() {
		removeEmailPanel();
		ep = new EMailPanel();
		ep.setUrls(urls);
		ep.setVisible(false);
		ep.setWidth("800px");
	}

	@PostConstruct
	protected void init() {
		loginButtons = VaadinUtils.horiz(login, create);
		login.addClickListener(loginListener());
		create.addClickListener(createListener());
		logout.setEnabled(false);
		logout.addClickListener(logoutListener());
		initEmailPanel();
		loginPanel = VaadinUtils.vert(userName, pwf);
		add(VaadinUtils.vert(panels = VaadinUtils.horiz(loginPanel, emailAndListUsersPanel = VaadinUtils.vert(ep)),
				VaadinUtils.horiz(loginButtons, logout)));
		add(status);

	}

	private ComponentEventListener<ClickEvent<Button>> logoutListener() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				logout.setEnabled(false);
				login.setEnabled(true);
				userName.setValue("");
				pwf.setValue("");
				removeEmailPanel();
				status.setText("Logged out");
				showEmailPanel(false);
				create.setVisible(true);
			}

		};
	}

	private void notifyUser(String msg) {
		Notification.show(msg);
	}
	
	private ComponentEventListener<ClickEvent<Button>> createListener() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (isEmpty(userName)) {
					notifyUser("Empty user name on create");
					return;
				}
				if (isEmpty(pwf)) {
					notifyUser("Password cannot be empty on create");
					return;
				}
				me = urls.createUser(userName.getValue(), pwf.getValue());
				if (me != null && me.isResult()) {
					login.setEnabled(false);
					logout.setEnabled(true);
					status.setText("Logged in new user as " + me.getName());
					showEmailPanel(true);
					create.setVisible(false);
				}
				handle(me);
			}

		};
	}

	public static boolean isEmpty(TextField tf) {
		String val = tf.getValue();
		if (val == null || val.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(PasswordField tf) {
		String val = tf.getValue();
		if (val == null || val.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	private ComponentEventListener<ClickEvent<Button>> loginListener() {
		return new ComponentEventListener<ClickEvent<Button>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentEvent(ClickEvent<Button> event) {
				if (isEmpty(userName)) {
					notifyUser("Empty user name on login");
					return;
				}
				if (isEmpty(pwf)) {
					notifyUser("Password cannot be empty on login");
					return;
				}
				
				me = urls.login(userName.getValue(), pwf.getValue());
				if (me != null && me.isResult()) {
					login.setEnabled(false);
					logout.setEnabled(true);
					create.setVisible(false);
					showEmailPanel(true);
					status.setText("Logged in existing user as " + me.getName());
				}
				handle(me);
			}

		};
	}

	private void showEmailPanel(boolean b) {
		if (b) {
			ep.setEmails(urls.getEmailsForUser(userName.getValue(), pwf.getValue(), 0L));
		}
		ep.setVisible(b);
	}

}
